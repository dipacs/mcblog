/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mc.mctwitter.rpc.auth.register;

import lombok.Getter;
import lombok.Setter;

/**
 *
 * @author david
 */
@Getter
@Setter
public class RegisterRpcRequest {
    
    private String name;
    private String email;
    private String password;
    
}
