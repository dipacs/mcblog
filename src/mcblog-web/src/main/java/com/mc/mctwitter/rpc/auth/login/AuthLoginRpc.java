/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mc.mctwitter.rpc.auth.login;

import com.mcblog.core.dao.objectify.AuthServiceFactory;
import com.mcblog.core.entity.Session;
import com.mc.mctwitter.rpc.ApiException;
import com.mcblog.core.service.AuthService;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;

/**
 *
 * @author david
 */
@Path("/auth")
@Consumes(value = "application/json")
@Produces(value = "application/json")
public class AuthLoginRpc {
    
    private AuthService authSvc = AuthServiceFactory.getInstance();
    
    @POST
    @Path("/login")
    public AuthLoginResponse login(AuthLoginRequest req) throws ApiException {
        String email = req.getEmail();
        if (email == null) {
            email = "";
        }
        
        String password = req.getPassword();
        if (password == null) {
            password = "";
        }
        
        Session ss = authSvc.login(email, password);
        if (ss == null) {
            throw new ApiException(401, "Invalid email or password!");
        }
        
        return new AuthLoginResponse(ss.getSsid());
    }
    
}
