/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mc.mctwitter.rpc.auth.register;

import com.mc.mctwitter.rpc.ApiException;
import com.mcblog.core.dao.objectify.AuthServiceFactory;
import com.mcblog.core.service.AuthService;
import java.util.HashMap;
import java.util.Map;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;

/**
 *
 * @author david
 */
@Path("/auth")
@Consumes(value = "application/json")
@Produces(value = "application/json")
public class AuthRegisterRpc {
    
    private AuthService authSvc = AuthServiceFactory.getInstance();
    
    @POST
    @Path("/register")
    public void register(RegisterRpcRequest req) throws ApiException {
        Map<String, String> fieldErrors = new HashMap<>();
        
        String email = checkEmail(req, fieldErrors);
        String name = checkName(req, fieldErrors);
        String password = checkPassword(req, fieldErrors);
        
        if (fieldErrors.size() > 0) {
            throw new ApiException(fieldErrors);
        }
        
        authSvc.register(name, email, password);
    }

    private String checkPassword(RegisterRpcRequest req, Map<String, String> fieldErrors) {
        String password = req.getPassword();
        if (password == null) {
            password = "";
        }
        password = password.trim();
        if (password.length() < 6) {
            fieldErrors.put("password", "The password must be minimum 6 characters!");
        }
        return password;
    }

    private String checkName(RegisterRpcRequest req, Map<String, String> fieldErrors) {
        String name = req.getName();
        if (name == null) {
            name = "";
        }
        name = name.trim();
        if (name.length() < 3) {
            fieldErrors.put("name", "The name must be minimum 3 characters!");
        }
        return name;
    }

    private String checkEmail(RegisterRpcRequest req, Map<String, String> fieldErrors) {
        String email = req.getEmail();
        if (email == null) {
            email = "";
        }
        email = email.trim().toLowerCase();
        if (email.length() < 5) {
            fieldErrors.put("email", "Invalid email address");
        }
        return email;
    }
    
}
