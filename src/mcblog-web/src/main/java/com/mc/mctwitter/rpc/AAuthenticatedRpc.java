/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mc.mctwitter.rpc;

import com.mcblog.core.dao.objectify.AuthServiceFactory;
import com.mcblog.core.entity.Session;
import com.mcblog.core.service.AuthService;

/**
 *
 * @author david
 */
public abstract class AAuthenticatedRpc {
    
    private AuthService authSvc = AuthServiceFactory.getInstance();
    
    public Session authenticate(String ssid) {
        Session ss = authSvc.findSessionById(ssid);
        if (ss == null) {
            throw new AccessDeniedException();
        }
        
        return ss;
    }
    
}
