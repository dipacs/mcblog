/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mc.mctwitter.rpc;

import com.mc.mctwitter.rpc.auth.login.AuthLoginRpc;
import com.mc.mctwitter.rpc.auth.logout.AuthLogoutRpc;
import com.mc.mctwitter.rpc.auth.register.AuthRegisterRpc;
import java.util.HashSet;
import java.util.Set;
import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

/**
 *
 * @author ipacsdavid
 */
@ApplicationPath("/rpc")
public class RpcRestServiceApplication extends Application {

    private Set<Object> singletons = new HashSet<Object>();

    public RpcRestServiceApplication() {
        // TODO register rpcs here
        singletons.add(new AuthRegisterRpc());
        singletons.add(new AuthLoginRpc());
        singletons.add(new AuthLogoutRpc());
    }

    @Override
    public Set<Object> getSingletons() {
        return singletons;
    }

}
