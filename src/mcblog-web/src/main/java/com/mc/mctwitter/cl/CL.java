/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mc.mctwitter.cl;

import com.googlecode.objectify.ObjectifyService;
import com.mcblog.core.entity.Blog;
import com.mcblog.core.entity.Follow;
import com.mcblog.core.entity.Post;
import com.mcblog.core.entity.Session;
import com.mcblog.core.entity.User;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

/**
 *
 * @author david
 */
public class CL implements ServletContextListener {

    @Override
    public void contextInitialized(ServletContextEvent sce) {
        ObjectifyService.register(User.class);
        ObjectifyService.register(Session.class);
        ObjectifyService.register(Blog.class);
        ObjectifyService.register(Post.class);
        ObjectifyService.register(Follow.class);
    }

    @Override
    public void contextDestroyed(ServletContextEvent sce) {
        // never called on appengine
    }
    
}
