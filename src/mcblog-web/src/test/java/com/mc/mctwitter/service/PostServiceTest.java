/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mc.mctwitter.service;


import com.mc.mctwitter.DatastoreMock;
import com.mcblog.core.dao.PostDao;
import com.mcblog.core.entity.Post;
import com.mcblog.core.service.PostService;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

/**
 *
 * @author david
 */
public class PostServiceTest {
    
    private final DatastoreMock helper = new DatastoreMock();
    
    public PostServiceTest() {
    }
    
    @Before
    public void setUp() {
        helper.setUp();
    }
    
    @After
    public void tearDown() {
        helper.tearDown();
    }

    @Test
    public void testFindPostById() {
        PostService ps = new PostService(new PostDao());
        ps.createPost(1, 12, "kuki", "maki");
        Post p = ps.findPostById(1);
        Assert.assertNotNull(p);
    }

    @Test
    public void testCreatePost() {
        Assert.fail("Test not implemented!");
    }

    @Test
    public void testListBlogPosts() {
        Assert.fail("Test not implemented!");
    }

    @Test
    public void testListPosts() {
        Assert.fail("Test not implemented!");
    }

    @Test
    public void testListFollowedPosts() {
        Assert.fail("Test not implemented!");
    }
    
}
