/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mcblog.core.dao.objectify;

import com.mcblog.core.dao.ISessionDao;
import com.googlecode.objectify.Key;
import com.googlecode.objectify.ObjectifyService;
import com.mcblog.core.entity.Session;

/**
 *
 * @author david
 */
public class SessionDao implements ISessionDao {
    
    public SessionDao() {
    }
    
    @Override
    public void save(Session session) {
        ObjectifyService.ofy().save().entity(session).now();
    }
    
    @Override
    public void delete(String ssid) {
        ObjectifyService.ofy().delete().key(Key.create(Session.class, ssid)).now();
    }
    
    @Override
    public Session findById(String ssid) {
        return ObjectifyService.ofy().load().now(Key.create(Session.class, ssid));
    }
}
