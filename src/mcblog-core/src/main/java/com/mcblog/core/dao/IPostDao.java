/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mcblog.core.dao;

import com.mcblog.core.entity.Post;
import java.util.List;

/**
 *
 * @author Kozma András
 */
public interface IPostDao {
    public Post findPostById(long postId);
    public Post save(Post post);
    public List<Post> listBlogPosts(long blogId, int offset, int limit);
    public List<Post> listPosts(int offset, int limit);
    public List<Post> listFollowedPosts(List<Long> followedBlogIds, int offset, int limit);
}
