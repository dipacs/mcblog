/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mcblog.core.dao;

import com.mcblog.core.entity.User;

/**
 *
 * @author david
 */
public interface IUserDao {
    public void save(User user);
    public User findByEmail(String email);
    public User findUserById(long userId);
}
