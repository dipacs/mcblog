/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mcblog.core.dao.objectify;

import com.mcblog.core.service.AuthService;

/**
 *
 * @author zsolt
 */
public class AuthServiceFactory {
    private static AuthService _instance = new AuthService(new UserDao(), new SessionDao());;
    
    public static AuthService getInstance() {
        return AuthServiceFactory._instance;
    }
}
