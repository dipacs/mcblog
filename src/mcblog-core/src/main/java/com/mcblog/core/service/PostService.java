/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mcblog.core.service;

import com.mcblog.core.dao.IPostDao;
import com.mcblog.core.entity.Blog;
import com.mcblog.core.entity.Follow;
import com.mcblog.core.entity.Post;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author david
 */
public class PostService {
    
    private final FollowService followSvc = new FollowService();
    private final BlogService blogSvc = new BlogService();
    
    private final IPostDao postDao;
    
    public PostService(IPostDao postDao) {
        this.postDao = postDao;
    }

    public Post findPostById(long postId) {
        return postDao.findPostById(postId);
    }

    public Post createPost(long userId, long blogId, String title, String content) {
        Blog blog = blogSvc.findBlogById(blogId);
        if (blog == null) {
            throw new IllegalArgumentException("Can't find blog with id: " + blogId);
        }
        
        if (blog.getOwnerId() != userId) {
            throw new IllegalArgumentException("User: " + userId + " isn't the owner of the blog: " + blogId);
        }
        
        Post post = new Post(null, title, content, blogId, userId);
        return postDao.save(post);
    }

    public List<Post> listBlogPosts(long blogId, int offset, int limit) {
        return postDao.listBlogPosts(blogId, offset, limit);
    }

    public List<Post> listPosts(int offset, int limit) {
        return postDao.listPosts(offset, limit);
    }

    public List<Post> listFollowedPosts(long userId, int offset, int limit) {
        List<Follow> follows = followSvc.listUserFollows(userId);
        List<Long> followedBlogIds = new ArrayList<>();
        for (Follow f : follows) {
            followedBlogIds.add(f.getBlogId());
        }
        
        if (followedBlogIds.size() < 1) {
            return new ArrayList<>();
        }
        
        return postDao.listFollowedPosts(followedBlogIds, offset, limit);
    }
    
}
