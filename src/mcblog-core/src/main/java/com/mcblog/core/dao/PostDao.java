/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mcblog.core.dao;

import com.googlecode.objectify.Key;
import com.googlecode.objectify.ObjectifyService;
import com.mcblog.core.entity.Post;
import java.util.List;

/**
 *
 * @author Kozma András
 */
public class PostDao implements IPostDao {

    @Override
    public Post findPostById(long postId) {
        return ObjectifyService.ofy().load().now(Key.create(Post.class, postId));
    }

    @Override
    public Post save(Post post) {
        ObjectifyService.ofy().save().entity(post).now();
        return post;
    }

    @Override
    public List<Post> listBlogPosts(long blogId, int offset, int limit) {
        return ObjectifyService.ofy().load().type(Post.class).filter("blogId", blogId).offset(offset).limit(limit).list();
    }

    @Override
    public List<Post> listPosts(int offset, int limit) {
        return ObjectifyService.ofy().load().type(Post.class).offset(offset).limit(limit).list();
    }

    @Override
    public List<Post> listFollowedPosts(List<Long> followedBlogIds, int offset, int limit) {
        return ObjectifyService.ofy().load().type(Post.class).filter("blogId", followedBlogIds).offset(offset).limit(limit).list();
    }
    
}
