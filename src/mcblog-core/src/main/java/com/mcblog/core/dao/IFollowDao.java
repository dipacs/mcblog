/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mcblog.core.dao;

import com.mcblog.core.entity.Follow;
import java.util.List;

/**
 *
 * @author Kassay Akos
 */
public interface IFollowDao {
    public void Save(Follow f);
    public void Delete(Follow f);
    public Follow GetFollowByUserIdAndBlogId(long blogId, long userId);
    public int GetFollowCountByBlogId(long blogId);
    public List<Follow> GetFollowByUserId(long userId);
}
