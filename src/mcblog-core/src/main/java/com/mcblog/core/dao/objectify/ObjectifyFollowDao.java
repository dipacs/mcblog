/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mcblog.core.dao.objectify;

import com.googlecode.objectify.ObjectifyService;

import com.mcblog.core.entity.Follow;
import java.util.List;
import com.mcblog.core.dao.IFollowDao;

/**
 *
 * @author Kassay Akos
 */
public class ObjectifyFollowDao implements IFollowDao {

    @Override
    public void Save(Follow f) {
        ObjectifyService.ofy().save().entity(f).now();
    }

    @Override
    public void Delete(Follow f) {
        ObjectifyService.ofy().delete().entity(f).now();
    }

    @Override
    public Follow GetFollowByUserIdAndBlogId(long blogId, long userId) {
        return ObjectifyService.ofy().load().type(Follow.class).filter("blogId", blogId).filter("userId", userId).first().now();
    }

    @Override
    public int GetFollowCountByBlogId(long blogId) {
        return ObjectifyService.ofy().load().type(Follow.class).filter("blogId", blogId).count();
    }

    @Override
    public List<Follow> GetFollowByUserId(long userId) {
        return ObjectifyService.ofy().load().type(Follow.class).filter("userId", userId).list();
    }
    
}
