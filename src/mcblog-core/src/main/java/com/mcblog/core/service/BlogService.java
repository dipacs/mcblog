/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mcblog.core.service;

import com.mcblog.core.dao.IBlogDao;
import com.mcblog.core.dao.IFollowDao;
import com.mcblog.core.entity.Blog;
import com.mcblog.core.entity.Follow;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author david
 */
public class BlogService {
    
    private FollowService followSvc;
    private IBlogDao blogDao;
    
    public BlogService(IBlogDao blogDao, IFollowDao followDao) {
        this.blogDao = blogDao;
        this.followSvc = new FollowService(followDao);
    }

    public Blog createBlog(long userId, String name, String content) {
        name = name.trim();
        content = content.trim();
        
        Blog blog = new Blog(null, name, content, userId, System.currentTimeMillis());
        blogDao.save(blog);
        
        return blog;
    }

    public Blog findBlogById(long blogId) {
        return blogDao.findBlogById(blogId);
    }

    public List<Blog> listOwnBlogs(long userId) {
        return blogDao.listOwnBlogs(userId);
    }

    public List<Blog> listFollowedBlogs(long userId, int offset, int limit) {
        List<Follow> follows = followSvc.listUserFollows(userId);
        
        List<Long> blogIds = new ArrayList<>();
        for (Follow follow : follows) {
            blogIds.add(follow.getBlogId());
        }
        
        return blogDao.listByIds(blogIds);
    }
    
}
