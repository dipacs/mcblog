/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mcblog.core.dao;

import com.mcblog.core.entity.Session;

/**
 *
 * @author david
 */
public interface ISessionDao {
    public void save(Session session);
    public void delete(String ssid);
    public Session findById(String ssid);
}
