/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mcblog.core.service;

import com.mcblog.core.dao.IFollowDao;
import com.mcblog.core.entity.Follow;
import java.util.List;

/**
 *
 * @author david
 */
public class FollowService {
    private IFollowDao followDao;
    
    public FollowService(IFollowDao followDao) {
        this.followDao = followDao;
    }

    public Follow followBlog(long blogId, long userId) {
        Follow follow = followDao.GetFollowByUserIdAndBlogId(blogId, userId);
        if (follow != null) {
            return follow;
        }
        
        follow = new Follow(null, blogId, userId, System.currentTimeMillis());
        followDao.Save(follow);
        
        return follow;
    }

    public void unfollowBlog(long blogId, long userId) {
        Follow f = followDao.GetFollowByUserIdAndBlogId(blogId, userId);
        if (f == null) {
            return;
        }

        followDao.Delete(f);
    }

    public int getFollowCount(long blogId) {
        return followDao.GetFollowCountByBlogId(blogId);
    }

    public List<Follow> listUserFollows(long userId) {
        return followDao.GetFollowByUserId(userId);
    }
    
}
