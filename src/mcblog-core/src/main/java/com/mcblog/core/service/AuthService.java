/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mcblog.core.service;

import com.mcblog.core.dao.ISessionDao;
import com.mcblog.core.dao.IUserDao;
import com.mcblog.core.entity.Session;
import com.mcblog.core.entity.User;
import java.util.UUID;

/**
 *
 * @author david
 */
public class AuthService {
    private final IUserDao userDao;
    private final ISessionDao sessionDao;
    
    public AuthService(IUserDao userSomething, ISessionDao sessionSomething) {
        this.userDao = userSomething;
        this.sessionDao = sessionSomething;
    }

    public Session login(String email, String password) {
        email = email.trim().toLowerCase();
        password = password.trim();
        
        User user = this.userDao.findByEmail(email);
        if (user == null) {
            return null;
        }
        
        if (!user.getPassword().equals(password)) {
            return null;
        }
        
        Session ss = new Session(UUID.randomUUID().toString(), user.getId(), System.currentTimeMillis());
        this.sessionDao.save(ss);
        
        return ss;
    }

    public void logout(String ssid) {
        this.sessionDao.delete(ssid);
    }

    public User register(String name, String email, String password) {
        if (name == null) {
            throw new NullPointerException();
        }
        
        name = name.trim();
        if (name.length() < 3) {
            throw new IllegalArgumentException("Minimum name length: 3");
        }
        
        if (email == null) {
            throw new NullPointerException();
        }
        
        email = email.trim().toLowerCase();
        if (email.length() < 5) {
            throw new IllegalArgumentException("Invalid email address");
        }
        
        if (password == null) {
            throw new NullPointerException();
        }
        
        password = password.trim();
        if (password.length() < 6) {
            throw new IllegalArgumentException("Minimum password length: 6");
        }
        
        User user = this.userDao.findByEmail(email);
        if (user != null) {
            throw new IllegalArgumentException("Email address already exists.");
        }
        
        user = User.builder()
                .email(email)
                .password(password)
                .name(name)
                .build();
        
        this.userDao.save(user);
        
        return user;
    }

    public Session findSessionById(String ssid) {
        return this.sessionDao.findById(ssid);
    }

    public User findUserById(long userId) {
        return this.userDao.findUserById(userId);
    }
    
    public User findUserByEmail(String email) {
        email = email.trim().toLowerCase();
        return this.userDao.findByEmail(email);
    }
    
}
