/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mcblog.core.dao.objectify;

import com.mcblog.core.dao.IUserDao;
import com.googlecode.objectify.Key;
import com.googlecode.objectify.ObjectifyService;
import com.mcblog.core.entity.User;

/**
 *
 * @author david
 */
public class UserDao implements IUserDao {
    
    public UserDao() {
    }
    
    @Override
    public void save(User user) {
        ObjectifyService.ofy().save().entity(user).now();
    }
    
    @Override
    public User findByEmail(String email) {
        return ObjectifyService.ofy().load().type(User.class).filter("email", email).first().now();
    }
    
    @Override
    public User findUserById(long userId) {
        return ObjectifyService.ofy().load().now(Key.create(User.class, userId));
    }        
}
