/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mcblog.core.dao.objectify;

import com.googlecode.objectify.Key;
import com.googlecode.objectify.ObjectifyService;
import java.util.List;
import com.mcblog.core.dao.IBlogDao;
import com.mcblog.core.entity.Blog;
import java.util.ArrayList;
import java.util.Map;

/**
 *
 * @author kassay Akos
 */
public class ObjectifyBlogDao implements IBlogDao {

    @Override
    public void save(Blog blog) {
        ObjectifyService.ofy().save().entity(blog);
    }

    @Override
    public Blog findBlogById(long blogId) {
        return ObjectifyService.ofy().load().now(Key.create(Blog.class, blogId));
    }

    @Override
    public List<Blog> listOwnBlogs(long userId) {
        return ObjectifyService.ofy().load().type(Blog.class).filter("userId", userId).list();
    }

    @Override
    public List<Blog> listByIds(List<Long> blogIds) {
        List<Key<Blog>> blogKeys = new ArrayList<>();
        for (long blogId : blogIds) {
            blogKeys.add(Key.create(Blog.class, blogId));
        }
        
        Map<Key<Blog>, Blog> resMap = ObjectifyService.ofy().load().keys(blogKeys);
        
        List<Blog> res = new ArrayList<>();
        res.addAll(resMap.values());
        
        return res;
    }
    
}
