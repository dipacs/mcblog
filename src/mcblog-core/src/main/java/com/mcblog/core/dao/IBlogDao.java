/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mcblog.core.dao;

import com.mcblog.core.entity.Blog;
import java.util.List;

/**
 *
 * @author kassay Akos
 */
public interface IBlogDao {
    public void save(Blog blog);
    public Blog findBlogById(long blogId);
    public List<Blog> listOwnBlogs(long userId);
    public List<Blog> listByIds(List<Long> blogIds);
}
